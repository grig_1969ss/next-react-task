import Main from "../components/Main";
import EditPage from "../components/EditPage";
import PersonalData from "../components/PersonalData";
import {createContext, useEffect, useState} from "react";


export const PersonalContext = createContext();

const Home = () => {
    const [show, setShow] = useState(true);

    const [data, setData] = useState({
        fullName: 'Иванова Анна Михайловна',
        email: "Ivanova@mail.ru",
        phone: 'Укажите номер телефона'
    });
    const [formData, setFormData] = useState({
        fullName: '',
        email: '',
        phone: ''
    });

    const createLocalStorage = () => {
        localStorage.setItem("user-data", JSON.stringify(formData))
    };

    useEffect(() => {
        const dataFromLocal = JSON.parse(localStorage.getItem("user-data"));
        if(dataFromLocal){
            setData(dataFromLocal)
        }
    },[formData]);


    const handleClick = () => {
        setShow(!show)
    };



    return (
        <PersonalContext.Provider value={{
            show,
            handleClick,
            data,
            createLocalStorage,
            formData,
            setFormData,
        }}>
            <div className={'app'}>
                <Main/>
                <PersonalData/>
                <EditPage/>
            </div>
        </PersonalContext.Provider>
    )
};

export default Home;