import Personal from "./Personal";
import {useContext} from "react";
import {PersonalContext} from "../pages";


const Main = () => {

    const {data} = useContext(PersonalContext);



    return (
        <div className={'MainWrapper'} >
            <div className={'not-img-name'}>
                <div className={'not-img'}>
                    <img src={"images/Vector (3).png"} alt=""/>
                </div>
                <img src={"images/image 3.png"} alt=""/>
                <p className={"fullName"}>{data.fullName.split(' ')[0]} {data.fullName.split(' ')[1][0]}.</p>
            </div>
            <div className={'personal-text'}>
                <p className={'personal'}>ЛИЧНЫЙ ПРОФИЛЬ</p>
                <p className={'secondText'}>Главная/Личный профиль</p>
            </div>
            <Personal/>

            <style jsx>{`
                .MainWrapper {
                     margin-top: 17px;
                     margin-left: 25px;
                }
                .personal{
                    margin-bottom: 0;
                    top: -86px;
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 18px;
                    line-height: 25px;
                    color: #FFFFFF;
                }
                
                .secondText{
                    margin-top: 10px!important;
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: normal;
                    font-size: 14px;
                    line-height: 19px;
                    color: #FFFFFF;
                }
                
                .not-img-name{
                    display: flex;
                    align-items: center;
                    justify-content: flex-end;
                    margin-top: 20px;
                }
                
                .not-img {
                    border-right: 1px solid #FFFFFF;
                    margin-right: 23px;
                    width: 47px;
                }
                
                .fullName {     
                    margin-left: 20px;
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 14px;
                    line-height: 19px;
                    color: #FFFFFF;
            
                }
            
                img:nth-child(2){
                    border-radius: 50%;
                }
                
                 @media(max-width: 768px){
                 .MainWrapper {
                         margin-left: 10px;
                  }
       
                    .personal{
                        font-size: 14px;
                        line-height: 19px;
                    }
        
            
                    .secondText{
                        font-size: 12px;
                        line-height: 16px;
                    }
                    
                    .not-img{
                        margin-right: 10px;
                        width: 30px;
                    }
                    
                    img{
                        width: 24px;
                        display: flex;
                    }
                    
                    img:nth-child(1){
                        width: 17px;
                    }
                    .fullName {  
                        display: none;
                    }
                    
                    }
    
            `}</style>
        </div>
    )
};

export default Main