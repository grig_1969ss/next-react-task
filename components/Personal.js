import {useContext} from "react";
import {PersonalContext} from "../pages";

const Personal = () => {

    const ShowContext = useContext(PersonalContext);


    return (
        <div className={'PersonalWrapper'}>

            <div className={'img-name'}>
                <img className={'imgname'} src={"images/image 3.png"} alt=""/>
                <p className={'nameText'}>{ShowContext.data.fullName}</p>
            </div>
            {
                ShowContext.show ? < div className="edit">
                        <p className={'editText'}>РЕДАКТИРОВАТЬ</p>
                        <button onClick={ShowContext.handleClick}>
                            <img className={'editImg'} src={'images/Vector (4).png'} alt=""/>
                        </button>
                    </div> :
                    <div className="edit-close">
                        <p className={'editText'}>Закрыть</p>
                        <button onClick={ShowContext.handleClick}>
                            <img src={"images/Vector (6).png"} alt=""/>
                        </button>
                    </div>
            }

            <style jsx>{`
                .PersonalWrapper {
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                    margin-top: 32px;
                    height: 128px;
                    background: linear-gradient(270deg, #1A78C2 0%, #1A78C2 101.06%);
                    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
                    border-radius: 10px;
                }
                
                             
                a{
                    height: 14px;
                }
                
                .img-name {
                    display: flex;
                    justify-content: space-between;
                    align-items: center;
                }
                
                .imgname {
                    width: 80px;
                    border-radius: 50%;
                    margin-left: 30px;
                }
                
                .nameText{
                    margin-left: 42px;
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 30px;
                    line-height: 41px;
                    color: #FFFFFF;
                }
                
                 .edit{
                    display: flex;
                    align-items: center;
                    margin-right: 28px;
                 }
                 .edit-close {
                    display: flex;
                    align-items: center;
                    margin-right: 28px;
                 }
                 
                 .editText{
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 14px;
                    line-height: 19px;
                    text-transform: uppercase;
                    color: #FFFFFF;
                }
                
                button{
                    background: none;
                    border: none;
                    outline: none;
                    cursor: pointer;   
                }
                
                 .editImg{
                    margin-left: 8px;
                  }
                  
                  @media(max-width: 768px){
                  .PersonalWrapper {
                        height: 71px;
                        margin-top: 10px;
                   }
                    .imgname{
                        width: 40px;
                        margin-left: 10px;
                    }
                    
                
                    .nameText{
                        font-size: 14px;
                        line-height: 19px;
                        margin-left: 10px;
                    }
        
                    .editImg{
                        margin-right: -20px;
                    }
                    .editText{
                        display: none;
                    }
        
            `}</style>
        </div>

    )
};

export default Personal;