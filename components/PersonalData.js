import {useContext} from "react";
import {PersonalContext} from "../pages";


const PersonalData = () => {
    const ShowContext = useContext(PersonalContext);

    if(ShowContext.show) {
        return (
            <div className={'Personal'}>
                <div className={'email'}>
                    <img src={"images/Vector (1).png"} alt=""/>
                    <p>{ShowContext.data.email}</p>
                </div>
                <span className={'line'}></span>
                <div className={'phone'}>
                    <img src={'images/Vector (2).png'} alt=""/>
                    <p>{ShowContext.data.phone}</p>
                </div>
                <style jsx>{`
                    .Personal {
                        height: 192px;
                        margin-top: 24px;
                        background: #FFFFFF;
                        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
                        border-radius: 10px;
                        display: flex;
                        flex-direction: column;
                        justify-content: space-evenly;
                        margin-left: 25px;
                        align-items: flex-start;
                    }
                     .line{
                        width:100%;
                        border: 1px solid #CAE7FE;
                    }
                    
                     div{
                        display: flex;
                        align-items: center;
                        margin-left: 77px;
                    }
        
                    p{
                        margin-left: 45px;
                        font-family: Open Sans;
                        font-style: normal;
                        height: 25px;
                        font-weight: normal;
                        font-size: 18px;
                        line-height: 25px;
                        color: #313131;
                    }
                    
                     @media(max-width: 768px){
                     .Personal{
                        margin-left: 10px;
                        height: 128px;
                     }
                        div{
                            margin-left: 10px;
                        }
                            
                        img{
                            width: 20px;
                        }
                             
                        p{
                            font-size: 14px;
                            line-height: 19px;
                            margin-left: 12px;
                        }
                        }
    
                `}</style>
            </div>
        )
    } else {
        return null
    }
};

export default PersonalData