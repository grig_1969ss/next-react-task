import {useContext, useState} from "react";
import {ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import {PersonalContext} from "../pages";


const EditPage = () => {
    const ShowContext = useContext(PersonalContext);
    const errorMessageFullName = ['Это поле обязательно для заполнения', 'Вы неверно указали имя'];
    const errorMessageEmail = ['Это поле обязательно для заполнения', "Вы неверно указали Email"];
    const errorMessagePhone = ['Это поле обязательно для заполнения', 'Минимальные числа должны быть 12', 'Вы неверно указали номер телефона'];



    const [error, setError] = useState(false);
    const [open, setOpen] = useState(false);
    const [successOpen, setSuccessOpen] = useState(false);
    const validateRules = {
        fullName: ['required', 'matchRegexp:(?=^.{0,40}$)^[a-zA-Z-]+\\s[a-zA-Z-]+$'],
        email: ['required', 'isEmail'],
        phone: ['required', 'minNumber:12', "matchRegexp:^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$"]
    };


    const handleOnChane = e => {
        ShowContext.setFormData({
            ...ShowContext.formData,
            [e.target.name]: e.target.value
        })
    };


    const handleSubmit = () => {
        if (!error) {
            setOpen(!error);
            ShowContext.createLocalStorage()
        }
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleCloseSuccess = () => {
        setSuccessOpen(false);
    };

    const makeToken = (length) => {
        let result = '';
        let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    };


    const sendData = () => {
        fetch("http://jsonplaceholder.typicode.com/posts", {
            method: 'POST',
            body: JSON.stringify(ShowContext.formData),
            headers: {
                'Content-Type': 'application/json',
                'x-token-access': makeToken(16)
            }
        })
            .then(res => res.json())
            .then(() => {
                setSuccessOpen(true)
            })
            .catch(err => console.log(err));

        setOpen(false);
        ShowContext.setFormData({
            fullName: '',
            email: '',
            phone: ''
        })
    };

    if (!ShowContext.show) {

        return (
            <div className={'EditPageWrapper'}>
                <ValidatorForm
                    className={'form'}
                    onSubmit={handleSubmit}
                    onError={() => setError(true)}
                >
                    <div className={'input-wrapper'}>
                        <div className={"item-wrap"}>
                            <img className={'first-img'} src={"images/Vector (5).png"} alt=""/>
                            <TextValidator
                                label="Фамилия и имя"
                                name={"fullName"}
                                validators={validateRules.fullName}
                                placeholder="Укажите ваши фамилию и имя"
                                errorMessages={errorMessageFullName}
                                variant="outlined"
                                value={ShowContext.formData.fullName}
                                onChange={handleOnChane}
                            />
                        </div>
                        <div className={"item-wrap"}>
                            <img className={'img'} src={"images/Vector (1).png"} alt=""/>
                            <TextValidator
                                label="E-mail"
                                name={"email"}
                                validators={validateRules.email}
                                placeholder="Ivanova@mail.ru"
                                variant="outlined"
                                errorMessages={errorMessageEmail}
                                value={ShowContext.formData.email}
                                onChange={handleOnChane}
                            />
                        </div>
                        <div className={"item-wrap"}>
                            <img className={'img'} src={"images/Vector (2).png"} alt=""/>
                            <TextValidator
                                label="Номер телефона"
                                errorMessages={errorMessagePhone}
                                name={"phone"}
                                validators={validateRules.phone}
                                placeholder="Укажите номер телефона"
                                variant="outlined"
                                value={ShowContext.formData.phone}
                                onChange={handleOnChane}
                            />
                        </div>
                    </div>
                    <div className={'buttonWrapper'}>
                        <button type={'submit'} className={'save-btn'}>Сохранить изменения</button>
                    </div>
                </ValidatorForm>

                <Dialog
                    className={'dialog1'}
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"

                >
                    <button onClick={handleClose} className={'close-btn'}>
                        <img src={"images/Vector (7).png"} alt=""/>
                    </button>
                    <DialogTitle id="alert-dialog-title"><p>Сохранить изменения?</p></DialogTitle>
                    <DialogActions>
                        <button className={'save-btn'} onClick={sendData}>Сохранить</button>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            Не сохранять
                        </Button>
                    </DialogActions>
                </Dialog>


                <Dialog
                    className={'dialog2'}
                    open={successOpen}
                    onClose={handleCloseSuccess}
                    aria-labelledby="alert-dialog"
                    id={"well-dialog"}
                >
                    <DialogTitle id="alert-dialog-title2"><p className={'well-title'}>Данные успешно сохранены</p>
                    </DialogTitle>
                    <DialogActions>
                        <Button onClick={handleCloseSuccess} color="primary" autoFocus className={"well-btn"}>
                            Хорошо
                        </Button>
                    </DialogActions>
                </Dialog>
                <style jsx>{`
                .EditPageWrapper {
                    height: 245px;
                    background: #FFFFFF;
                    box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
                    border-radius: 10px;
                    margin-top: 24px;
                    margin-left: 25px;
                    
                }
                
                .input-wrapper {
                    display: flex;
                }
                
                .item-wrap {
                    display: flex;
                    align-items: center;
                    border-right: 1px solid #CAE7FE;
                    padding: 23px 77px 23px 0;
                    margin-left: 34px;
                }
                
                .item-wrap:last-child { 
                    border-right: 0;
                    padding-right: 0;
                }
                
                .first-img {
                   margin-left: 35px; 
                   margin-right: 46px;
                }
                .img {
                    margin-left: 32px; 
                    margin-right: 46px;
                    
                }
                
                button{
                    font-family: Open Sans;
                    font-style: normal;
                    font-weight: 600;
                    font-size: 14px;
                    line-height: 19px;
                    color: #FFFFFF;
                    border: none;
                    outline: none;
                    background: #01BDA7;
                    border-radius: 36px;
                    padding: 15px;
                    margin-top: 15px;
                }
                
                .buttonWrapper {
                    width: 100%;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
                
                .save-btn {
                    width: 212px;
                    
                }
                
                 @media(max-width: 1024px){
                        .item-wrap{
                            padding: 23px 40px 23px 0;
                            margin-right: 20px;
                            margin-left: 0;
                        }
                        
                        img{
                            margin-right: 15px;
                       }
                 }
                 
                 @media(max-width: 768px){
                    .EditPageWrapper {
                        height: unset;
                        margin-left: 10px;
                        margin-top: 10px;
                        background: #FFFFFF;
                        box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.15);
                        
                    }
                    
                    .input-wrapper {
                        display: flex;
                        flex-direction: column;
                        align-items: center;
                    }
                    
                    .item-wrap{
                        border: none;
                        padding-right: 0;
                        margin-right: 0;
                    }
                    
                    img{
                        display: none;
                    }
                    
                    .save-btn{
                        margin-bottom: 32px;
                    }
                    
                 }
    
             `}</style>
                <style global jsx>{`
              .form{
                    height: 100%;
                    display: flex;
                    justify-content: space-evenly;
                    flex-direction: column;
                }
                
                
             `}</style>
            </div>
        )
    } else {
        return null
    }
};

export default EditPage